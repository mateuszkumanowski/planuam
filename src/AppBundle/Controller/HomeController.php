<?php
/**
 * Created by PhpStorm.
 * User: MarcinB
 * Date: 2016-01-01
 * Time: 15:58
 */

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class HomeController extends Controller{
    /**
     * @Route("/", name="home")
     */
    public function indexAction(Request $request)
    {

        // replace this example code with whatever you need
        return $this->render('default/header.html.twig', [
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            'dataList' => $this->getAllData(),
        ]);
    }
    public function getAllData(){
        $listaZjazdow = simplexml_load_file('doc.xml');

        $arrayListDay = array();

        foreach($listaZjazdow as $dzien){


                array_push($arrayListDay,$dzien);

        }
        var_dump($listaZjazdow);
        return $arrayListDay;
    }
} 