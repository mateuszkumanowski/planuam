<?php

/*Created By Mateusz Kumanowski*/
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        if(isset($_GET['viewType'])){
            if($_GET['viewType']=='calendar'){
                $templateName= 'default/index.html.twig';
            }
            else{
                $templateName= 'default/'.$_GET['viewType'].'.html.twig';
            }
        }
        else{
            $templateName= 'default/index.html.twig';
        }

    if(isset($_GET["y"]) and $_GET["y"]!= "")
    {
        if($_GET["y"]<10)
        {
            $currentMonth =  substr($_GET["y"], -1);
        }else{
            $currentMonth = substr($_GET["y"], -2);}
        }
    else{
        $currentMonth =  date("m");}

        // replace this example code with whatever you need
        return $this->render($templateName, [
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            'dataCalendar' => $this->printCalendarArray(),
            'currentMonth' => $currentMonth,
            'headerData' => $this->getHeader()
        ]);
    }

    public function getHeader(){
        try{
            $headerData = simplexml_load_file('doc.xml');

        }catch(Exception $ex){}
        return $this->xml2array($headerData,null);
    }
    public function xml2array ( $xmlObject, $out = array () )
    {
        foreach ( (array) $xmlObject as $index => $node )
            $out[$index] = ( is_object ( $node ) ) ? $this->xml2array ( $node ) : $node;
        return $out;
    }

    public function tableRow($wydzial, $rok, $dzienVal){
        $row = $this->getDayList($wydzial,$rok,$dzienVal);
        return $row;
    }

    public function dateName($dzienVal){
        return date("l",strtotime($dzienVal));
    }

    public function getDayList($wydzial, $rok, $dzienVal){
        try{
        $dzienZjazdu = simplexml_load_file('doc.xml');
        $year = 'year'.$rok;
        $listaZjazdow = $dzienZjazdu->$wydzial->$year;
        $arrayListDay = array();

        foreach($listaZjazdow->day as $dzien){

            if($dzien['when'] == $dzienVal){
                array_push($arrayListDay,$dzien);
            }
        }
        }catch(Exception $ex){}
        return $arrayListDay;
    }

    public function dzien_tyg_nr($mies,$rok)
    {
        $dzien = date("N", mktime(0,0,0,$mies,1,$rok));
        return $dzien;
    }

    public function dni_mies($mies,$rok)
    {
        $dni = 31;

        while (!checkdate($mies, $dni, $rok))
            $dni--;
        return $dni;
    }

    public function printRows(){

        //zmienne kalendarza
        $nr_kratki=1;


        if(isset($_GET['y']))
        {
            $rok = substr($_GET['y'], 0, 4);
            $miesiac = substr($_GET['y'], -2);
            var_dump($rok);
        }else{
            $rok = date("Y");
            $miesiac = date("m");
        }
        $aktualna=date("d-m-Y");
        if(isset($_GET['studia']) && $_GET['studia'] != '')
        {
            $studia = $_GET['studia'];
        }
        else{

        }
        if(isset($_GET['rokstudiow']))
        {
            $rokstudiow = $_GET['rokstudiow'];
        }
        else{

        }

        $out = '
        <table class="table table-striped" style="width:100%">
        <thead>
        <tr style="height:20px">
            <th style="width:14%">Poniedziałek</th>
            <th style="width:14%">Wtorek</th>
            <th style="width:14%">Środa</th>
            <th style="width:14%">Czwartek</th>
            <th style="width:14%">Piątek</th>
            <th style="width:14%">Sobota</th>
            <th style="width:14%">Niedziela</th>
        </tr>
        </thead>
        <tbody>
        <tr>';
        for($i=1;$i<$this->dzien_tyg_nr($miesiac,$rok);$i++) //ile pustych pol
        {
            $out .= '<td>&nbsp;</td>'; $nr_kratki++;}
        for($i=1;$i<$this->dni_mies($miesiac,$rok) +1;$i++) //ile pelnych pol
        {
            $out .= '<td>';
            $out .= '<p>'.$i.'</p>';
            $out .= '<br>';
            $nr_kratki++;

            if($i<10)
                $i= '0'.$i;

            $dateAkt = $rok.'-'.$miesiac.'-'.$i;

            $out .= $this->tableRow($studia,$rokstudiow,$dateAkt);

            $out .= '</td>';

            if($nr_kratki%8=='0')
            {
                $out .= '</tr><tr>';
                $nr_kratki='1';
            }
        }

        for($i=$nr_kratki;$i<8;$i++) //ile pustych pol
        {
            $out .= '<td>&nbsp;</td>';
        }

        $out .= '</tr></tbody></table>';

        return $out;
    }

    public function printCalendarArray(){

        //zmienne kalendarza
        $nr_kratki=1;
        if(isset($_GET['y']) and $_GET['y'] != '' )
        {
            $rok = substr($_GET['y'], 0, 4);
            $miesiac = substr($_GET['y'], -2);
        }else{
            $rok = date("Y");
            $miesiac = date("m");
        }
        if(isset($_GET['studia']) && $_GET['studia'] != '')
        {
            $studia = $_GET['studia'];
        }
        else{
            $studia = "ZLMAT";
        }
        if(isset($_GET['rokstudiow']))
        {
            $rokstudiow = $_GET['rokstudiow'];
        }
        else{
            $rokstudiow = 1;
        }
        $out= array();
        for($i=1;$i<$this->dzien_tyg_nr($miesiac,$rok);$i++) //ile pustych pol
        {
            array_push($out,"");
            $nr_kratki++;
        }
        for($i=1;$i<$this->dni_mies($miesiac,$rok) +1;$i++) //ile pelnych pol
        {
            $nr_kratki++;

            if($i<10)
                $i= '0'.$i;

            $dateAkt = $rok.'-'.$miesiac.'-'.$i;

            array_push($out, $this->tableRow($studia,$rokstudiow,$dateAkt));


            if($nr_kratki%8=='0')
            {
                $nr_kratki='1';
            }
        }

        return $out;
    }



}
